<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script
      src="https://code.jquery.com/jquery-3.4.1.min.js"
      integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
      crossorigin="anonymous"></script>

    <script src="https://cloud.tinymce.com/5/tinymce.min.js"></script>
    <!-- <script>tinymce.init({selector:'textarea'});</script> -->
    <script>
    $(document).ready(function(){
    $("#clear").click(function(){
        $("#stuff").empty();
    });
    });
    </script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <style type="text/css">
        .box {
            border: 1px inset #ccc;
            width: 80em;
        }
        textarea {
            width:100%;
            height:20em;
        }
    </style>
</head>
<body class="container-fluid">

    <h1>UDF Exporter and Starter SQL Generator</h1>
    <ul>
        <li>Takes the Broker One UDF template text</li>
        <li>Produces the converted EpicAssure UDF template text</li>
        <li>Produces the starter SQL needed for the query</li>
    </ul>
    <?php
    //load the function to convert the TSV data to an array
    require_once 'tsv-to-array.inc.php';

    if (isset($_POST['stuff'])) {
        $stuff = $_POST['stuff']; 
    } else {
        $stuff = '';
    }
    $data = NULL;
    $udf_of_file = convert_broker1_to_assure($stuff);
    $udf = convert_from_pattern($udf_of_file);
    $fixed_udf = fix_tabs(clean(br_to_newline($udf['result'])));
    $psbl_missed = unrecognised_udf_code_check($fixed_udf);
    $psbl_missed_codes = $udf['codes'];
    $psbl_wrong_codes = $udf['psbl_bad'];
    ?>

    <h2>Broker One UDF Text</h2>
    <p>
        To convert, paste the Broker One UDF text <em>with 4D Write placeholder codes</em> 
        below and select the Convert button. <br>
        Placeholder codes are delimited by the "«" and "»" characters, e.g. «[Accounts]Name for Printing».
    </p>
    
    <div class="row">
        <div class="<?php if (count($psbl_missed_codes[0]) > 0) { echo "col-lg-6"; } else {echo "col-lg-12"; } ?>">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                <textarea name="stuff" id="stuff" rows="10" class="form-control"><?php echo $stuff ?></textarea>
                <br>
                <input type="submit" value="Convert" class="btn btn-primary">
                <input type="reset" value="Clear" id="clear" class="btn btn-secondary">
            </form>
        
        </div>
        <?php if (count($psbl_missed_codes[0]) > 0) { ?>
            <div class="col-lg-6">
                <?php
                echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">';
                echo "<h6>Unrecognised 4D Write Placeholder Codes: " . count($psbl_missed_codes[0]) . "</h6>";
                echo "<ul>";
                foreach ($psbl_missed_codes[0] as $key => $value) {
                    echo '<li>'.$value.'</li>';
                }
                echo "</ul>";
                echo "<h6>Generated Replacement Codes</strong></h6>";
                echo "<p><em>Please confirm via the EpicAssure Editor expression builder that the <strong>table, 
                    field</strong> and optional <strong>formatting</strong> strings making up the replacement code(s) <!-- - formatted as 
                    </em>[[(table_name.Field_Name)((format))]]<em> - --> are correct.</em></p>";
                echo "<ul>";
                foreach ($psbl_wrong_codes as $key => $value) {
                    echo '<li>'.$value.'</li>';
                }
                echo "</ul>";
                echo "<p>Check your input text for unusual characters to possibly fix the issue. 
                    There is a data file ('bk1_ea_doc_codes.tsv') which stores the old B1 placeholder codes with the matching EA placeholder codes.  
                    The idea is to copy the unrecognised placeholder code along with the correct replacement code 
                    (from the expression builder) into the data file using MS Excel or Libreoffice Calc
                    to edit the data file.  The  data file is found in the folder containing this script.</p>";
                // echo "<p>Edit the code list file to add the missing codes along with th</p>";
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
                ?>
            </div>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <h2>Converted UDF Text</h2>
            <p>Copy the content below and paste into UDF template editor.</p>
            <?php
                echo '<textarea readonly class="form-control">'.trim($fixed_udf).'</textarea>';
            ?>
        </div>
        <div class="col-lg-6">
            <h2>Starter SQL</h2>
            <p>Edit and/or Copy the content below and paste where appropriate.</p>
            <?php 
                $partial_sql = generate_partial_sql($fixed_udf);
                echo "<textarea class='form-control'>".trim($partial_sql)."</textarea>";
                // echo generate_partial_sql($fixed_udf);
            ?>
        </div>    
    </div>

    <?php
        /**
         * Returns the character encoding of a given string. Used to detect that of the 4D code. Don't quite remember where I got it from.
         */
        function mb_detect_encoding1 ($string, $enc=null, $ret=null) { 
                
                static $enclist = array( 
                    'UTF-8', 'ASCII', 
                    'ISO-8859-1', 'ISO-8859-2', 'ISO-8859-3', 'ISO-8859-4', 'ISO-8859-5', 
                    'ISO-8859-6', 'ISO-8859-7', 'ISO-8859-8', 'ISO-8859-9', 'ISO-8859-10', 
                    'ISO-8859-13', 'ISO-8859-14', 'ISO-8859-15', 'ISO-8859-16', 
                    'Windows-1251', 'Windows-1252', 'Windows-1254', 
                    );
                
                $result = false; 
                
                foreach ($enclist as $item) { 
                    $sample = iconv($item, $item, $string); 
                    if (md5($sample) == md5($string)) { 
                        if ($ret === NULL) { $result = $item; } else { $result = true; } 
                        break; 
                    }
                }
                
            return $result; 
        } 
        
        
        /**
         * Remove unwanted characters
         */
        function clean($string){
            $string = preg_replace('/\\000/','',$string);
            $string = preg_replace('/\Â/','',$string);
            return $string;
        }
        
        /**
         * Returns text with the Broker One codes converted to EpicAssure based on translation data in TSV file
         */
        function convert_broker1_to_assure($stuff){
            
            $output = '';
            
            $file = 'bk1_ea_doc_codes.tsv';
            $data = tsv_to_array($file, array('header_row'=>false,'remove_header_row'=>false));
            
            $replacements = [];

            // convert text lines to array
            $text = clean(trim($stuff));
            $textAr = explode("\n", $text);

            // replace b1 codes with EA codes
            foreach ($textAr as $line) {
                
                foreach ($data as $thing){
                    if (count($thing) > 1){
                        // remove troublesome nonprinting 4D characters
                        $find = clean(utf8_encode($thing[0])); 
                        $replace = clean(utf8_encode($thing[1]));
                                                
                        if (($thing[0]) && ($thing[1]) && !empty($line) ) {
                            $count = substr_count($line, $find);
                            if ($count > 0){
                                $line = str_replace($find, $replace, $line);
                                $line = str_ireplace("\r", "\n", $line);
                                $line = preg_replace("/\n/", '', $line, 1);
                                array_push($replacements, $replace); // add elements
                            }
                        }
                    }                    
                }
                $output .= $line. "";
            } 
            return $output; 
        }

        /** 
        * @Returns first table name detected 
        */
        function getTableName($string){
            preg_match('~([a-z_]*)\.~', $string, $matches);
            return $matches[1];
        }
        
        /**
         * @Returns partial sql meant to give a head start
         */
        function generate_partial_sql($stuff) {
            preg_match_all('~\(([a-z_]*\.[a-zA-Z_]*)\)~', $stuff, $m );
            
            $rv = '';
            
            $forbidden = array(
                "users.User_Signature",
                "users.Full_Name", 
                "users.Salutation_Name", 
                "users.Title_Or_Position",
                "users.User_Code",
                "users.Department",
                "master_config.Business_Header_Image",
                "master_config.Business_Footer_Image"
            );

            $forbidden_tables = array(
                "users", "master_config"
            );
    
            $table = NULL;
            $joins = [];
            $i=0;
            foreach ($m as $val) {
                $val = array_diff($val, $forbidden);
                $val = array_unique($val);
                if (($i<1) && (!empty($val))){
                    preg_match('~\(([a-z_]*)\.~', $val[0], $matches);
                    $table = $matches[1];
                }
                
                if ($i >= 1) {
                    $rv .= "select \n";
                    $j=0;
                    foreach($val as $item){
                        $rv .= $item;
                        if ((!isset($main_table)) && (!in_array($item, $forbidden_tables))){
                            $main_table = $item;
                        }
                        if ($item == end($val)) {
                            $rv .= " \n";
                        } else {
                            $rv .= ", \n";
                        }

                        // get first table name detected as main SQL query table
                        // TODO: make selectable
                        $item_table = getTableName($item);
                        if (!in_array($item_table, $joins)){
                            $joins[] = $item_table;
                        }
                        $j++;
                    }
                    $rv .= "from " ;
                    if (isset($table)){
                        $rv .= $table;
                    }
                    $rv .= " ";
                    if (count($joins) >= 2){
                        foreach($joins as $join){
                            $rv .= "\nleft join " . $join . " on " . $table . ".[insert_join_field] = " . $join . ".[insert_join_field]";
                            $rv .= "";
                        }
                    }
                    $rv .= "\nwhere " ;
                    if (isset($table)){
                        $rv .= $table;
                        $rv .= '.fieldname = value';
                    }
                    $rv .= " ";
                    
                }            
                $i++;
            }    
            
            return $rv;
        }
        
        function br_to_newline($string){
            return str_replace('<br>', "\n\r", $string);
        }
        
        function fix_tabs($string){
            return str_replace("\t", "\t", $string);
        }
        
        function unrecognised_udf_code_check($string){
            return substr_count($string, '«');
        }
        
        /**
         * Returns an array of delimited strings matching a given pattern
         */
        function extract_delimited_codes($string, $start, $matching_pattern, $end){
            preg_match_all(
                '/'.$start.$matching_pattern.$end.'/',
                $string,
                $matches,
                PREG_PATTERN_ORDER
            );
            return $matches;
        }

        /**
         * @Returns converted table/field reference format from the BrokerOne editor format to EpicAssure editor format
         */
        function convert_tbl_fld_ref($tbl_fld_ref){
            $re = '/([a-zA-Z ]*)\.([a-zA-Z ]*).?(\(\(([$#., 0;-]*)\)\))?]]/m';
            $str = $tbl_fld_ref;
            $subst = '\\1|\\2)|\\4';
            
            $result = preg_replace($re, $subst, $str);
            $field_ref = explode("|", $result);

            $table = strtolower($field_ref[0]);
            $field = $field_ref[1];
            $table_field = $table.".".$field;
            $table_field = str_replace(" ", "_", $table_field);
            echo $field_ref[2];
            if (!empty($field_ref[2])){
                return $table_field.'(('.$field_ref[2].'))]]';
            } else {
                return $table_field.']]';
            }
        }

        /**
         * @Returns text with converted table/field reference codes converted based pattern 
         */
        function convert_from_pattern($text){
            $text = clean(utf8_encode(trim($text)));
            $rv = [];
            $re = '/\[([a-zA-z ]*)\]([a-zA-z ]*):?([A-Za-z $#,0-9.;\-\(\)]*)]*/m';
            $subst = '[[(\1.\2)((\3))]]';
            $result = preg_replace($re, $subst, $text);
            $result = str_replace('(())', '', $result );
            $codes = extract_delimited_codes($result, '\[\[\(', '([a-zA-Z ]*)\.([a-zA-Z ]*):?([A-Za-z0-9 $#,.;\-\(\)]*)', '\)\]\]'); // make sure to escape delimiter chars as will be processed via regex
            $converted_codes = [];
            foreach ($codes[0] as $k=>$code) {
                $converted_code = convert_tbl_fld_ref($code);
                $converted_codes[] = $converted_code;
                $result = str_ireplace($code, $converted_code, $result);
            }
            $unrecognised_codes = extract_delimited_codes($text, '«', '(.*?)', '»');
            $result = str_replace('«', '', $result );
            $result = str_replace('»', '', $result );
            
            $rv['result'] = $result;
            $rv['codes'] = $unrecognised_codes;
            $rv['psbl_bad'] = $converted_codes; 
            return $rv;
        }

    ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>