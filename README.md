﻿# UDF Exporter and Starter SQL Generator
Reduce the time it takes to convert a Broker One UDF template to an EpicAssure template.

## How it works
* Accepts source text with placeholder codes from the 4D Write editor.
* Parses the source text checking for B1 placeholder codes to match against confirmed EA placeholder codes.
* Generates a "best guess" suggestion for unrecognized placeholder codes.
* Produces the converted EpicAssure UDF template text with confirmed and "best guess" placeholder codes in place.
* Produces the starter SQL needed for the query

## Challengess
* The table names between the 4D and MySQL datasets are not always consistent
* The BrokerOne formatting options for the placeholder data aren't always clearly mapped to those in EpicAssure
* A number of non-printing characters including the delimiters requiring character set conversion and filtering

## Prerequisite Knowledge 
_The following is out of the scope of this document and may be found in the respective KB documentation._

* Cloning/downloading from GIT repositories
* How to open/edit UDF templates in BrokerOne 
* How to open/create UDF templates in EpicAssure. 

## Setup and Launch (Windows)
* Clone or download the files from the repository.
 * To clone (must have git installed), type the following at the command prompt:
 > `git clone https://vrowe@bitbucket.org/vrowe/udf-migrator.git udf-migrator`

* Navigate to the udf-migrator folder 
- Run the "launch.bat" file.

## Usage Instructions
* Open the UDF template in BrokerOne.
* Ensure the placeholder codes are visible via the **[ ]** toolbar button which is shaded when enabled.
* Copy the UDF text with 4D Write placeholder codes from the BrokerOne editor.
* Paste it into the Broker One UDF Text form field.
* Click the **Convert** button.
* A notification will be shown if there are any issues with placeholder codes it is not sure of. Where these exist it will give you its best guess based on the BrokerOne table name. It will also give instructions on how to update the data file with the correct EpicAssure placeholder codes.
 - After any updates, click the **Convert** button to restart the conversion process.
* If there are no issues with what is produced, copy & paste the converted document text into the EpicAssure editor.
* Select Close/Save which closes the editor popup modal.
* Select Save button to save the record.

## To Do
* Add functionality to confirm a "best guess" placeholder code to the data file.
* Add functionality to manually add the correction for an unrecognized placeholder code to the data file.


